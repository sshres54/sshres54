package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void testBoxComplete() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        // Draw the lines surrounding a box
        grid.drawHorizontal(0, 0, 1);
        grid.drawHorizontal(0, 1, 1);
        grid.drawVertical(0, 0, 1);
        grid.drawVertical(1, 0, 1);

        // Check if the box is complete
        assertTrue(grid.boxComplete(0, 0), "Box should be complete");

        // Draw additional lines for another box
        grid.drawHorizontal(1, 0, 1);
        grid.drawHorizontal(1, 1, 1);
        grid.drawVertical(2, 0, 1);
        grid.drawVertical(2, 1, 1);

        // Check if the new box is complete
        assertTrue(grid.boxComplete(1, 0), "Box should be complete");
    }

    @Test
    public void testDrawHorizontalLineAlreadyDrawn() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        // Draw a horizontal line
        grid.drawHorizontal(0, 0, 1);
        // Attempt to draw the same line again, which should throw an exception
        IllegalStateException thrown = assertThrows(IllegalStateException.class, () -> grid.drawHorizontal(0, 0, 1));
        logger.info(thrown.getMessage());
    }

    @Test
    public void testDrawVerticalLineAlreadyDrawn() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2);

        // Draw a vertical line
        grid.drawVertical(0, 0, 1);
        // Attempt to draw the same line again, which should throw an exception
        IllegalStateException thrown = assertThrows(IllegalStateException.class, () -> grid.drawVertical(0, 0, 1));
        logger.info(thrown.getMessage());
    }
}
